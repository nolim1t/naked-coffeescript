**What**
------------
This is a bare bones coffeescript+express powered server (foreman/heroku ready) to save you time.

**Why**
------------
Society (particularly in tech moves really fast). Nowadays it seems a LOT of people have ideas, and there is a lack of time to do them and launch quick.

So I've decided to copy one of my past projects which was *roughly structured* in this format and create a template for people to use (pull requests welcome!).

So you can build, launch, or fail (not in that order) faster so you can move on to the next best thing. Unless of course the coffeescript fad passes haha

**How**
------------
> 1. Clone this project
> 2. Do a **git archive --format=tar --prefix=AppName/ HEAD | (cd /App/BasePath && tar xf -)**
> 3. Do a **git init** and create a new repo for your project
> 4. Make changes and break stuff

**Example app**
------------
> Just to prove that this is running, there is an example app at http://naked-coffeescript.herokuapp.com/

**Suggestions**
------------
If there are any issues, I do expect to see a pull request sent or issue logged, or everythings perfect (which it isn't)